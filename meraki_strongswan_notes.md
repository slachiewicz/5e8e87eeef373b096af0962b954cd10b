These are my notes for connecting to a meraki client vpn from ubuntu 16.04. This configuration assumes you are using a psk for the ipsec auth.

Install the following packages:

```
apt-get install -y strongswan xl2tpd
```

Configure strong swan
```
cat > /etc/ipsec.conf <<EOF
# ipsec.conf - strongSwan IPsec configuration file

# basic configuration

config setup
        # strictcrlpolicy=yes
        # uniqueids = no

# Add connections here.

# Sample VPN connections

conn %default
        ikelifetime=60m
        keylife=20m
        rekeymargin=3m
        keyingtries=1
        keyexchange=ikev1
        authby=secret
        ike=aes128-sha1-modp1024,3des-sha1-modp1024!
        esp=aes128-sha1-modp1024,3des-sha1-modp1024!

conn meraki-vpn
     keyexchange=ikev1
     left=%defaultroute
     auto=add
     authby=secret
     type=transport
     leftprotoport=17/1701
     rightprotoport=17/1701
     # set this to the ip address of your meraki vpn  
     right=XXX.XXX.XXX.XXX
EOF

cat > /etc/ipsec.secrets <<EOF
: PSK "YOUR_PSK_GOES_HERE"
EOF

```

Configure xl2tp:
```
cat > /etc/xl2tpd/xl2tpd.conf <<EOF
[lac meraki]
# your meraki vpn ip goes here
lns = XXX.XXX.XXX.XXX
ppp debug = yes
pppoptfile = /etc/ppp/options.l2tpd.client
length bit = yes
EOF

cat > /etc/ppp/options.l2tpd.client <<EOF
ipcp-accept-local
ipcp-accept-remote
refuse-eap
require-mschap-v2
noccp
noauth
idle 1800
mtu 1410
mru 1410
defaultroute
usepeerdns
debug
lock
connect-delay 5000
EOF

mkdir -p /var/run/xl2tpd
touch /var/run/xl2tpd/l2tp-control
```

Restart your services:
```
service strongswan restart
service xl2tpd restart
```

Start the ipsec connection:
```
ipsec auto --up meraki
```

Start the l2tp connection (with your username and password)
```
echo "c meraki <user> <pass>" > /var/run/xl2tpd/l2tp-control
```

Add a route for your internal network:
```
ip route add 10.0.0.1/24 dev ppp0
```

To disconnect:
```
echo "d meraki" > /var/run/xl2tpd/l2tp-control
ipsec down meraki
```

